<?php

/*
|--------------------------------------------------------------------------
| Register The Composer Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so that we do not have to worry about the
| loading of any our classes "manually". Feels great to relax.
|
*/

require __DIR__.'/vendor/autoload.php';

use Baranowski\Dog;
use Baranowski\Cat;

//11
$dog = new Dog("Santa's Little Helper",rand(1,5));
$cat = new Cat("Snowball II",rand(1,5));

//12
print "{$dog->name()} is currently {$dog->age()} years old. \r\n";

//13
print "{$dog->name()} says {$dog->speak("meow")}? Nah, {$dog->name()} says {$dog->speak()}! {$dog->name()} is {$dog->age()}. \r\n";

//14
print "The cat's name is {$cat->name()}.\r\n";

//15
$cat->setName("Garfield");

//16
print "The cat's name has been changed to {$cat->name()}. The average length of the cat's name is {$cat->getAverageNameLength()}.\r\n";